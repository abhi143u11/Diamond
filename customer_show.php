<?php
require_once('classes/class.database.php');
require_once('classes/class.customer.php');

$customer_list=new Ds_Customer();
$result=$customer_list->all_list_customer();



?>
<?php include('header.php'); ?>
        <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo CUSTOMERALL;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-striped">
                  <thead>
<tr><th>Name</th><th>Address</th><th>Phone</th><th>Company Name</th><th>Edit</th><th>Delete</th></tr></thead>
<?php
    
foreach($result as $obj)
{
    echo "<tr><td>".$obj['name']."</td><td>".$obj['address']."</td><td>".$obj['phoneno']."</td><td>".$obj['companyname']."</td>
    <td><a href='customer_edit.php?cid=".$obj['cid']."'><i class=\"fa fa-edit\"></i></a></td>
    <td><a href='customer_delete.php?cid=".$obj['cid']."'  onclick=\"return confirm('Really delete?');\"><i class=\"fa fa-trash text-red\"></i></a></td>
    </tr>";
}    
    
?>
</table>
</div>
</div>
</section>
<?php include('footer.php');

if(isset($_SESSION)){
 ?>

 <script>
      $(function () {
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });
      });
      
     
    var notify = $.notify('', {
    type: '<?php echo $_SESSION['type']; ?>',
    allow_dismiss: true,
    showProgressbar: false,
    placement: {
        from: "top",
        align: "right"
    },
});

setTimeout(function() {
    notify.update('message', '<?php echo $_SESSION['message']; ?>');
}, 1000);
   </script>
      
   <?php 
   session_destroy();
}
    ?>
  </body>
</html>



