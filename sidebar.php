  <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
         
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i> <span>Customer</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="customer_add.php"><i class="fa fa-circle-o"></i>Add Customer</a></li>
                <li><a href="customer_show.php"><i class="fa fa-circle-o"></i>View Customer</a></li>
              </ul>
            </li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dollar"></i> <span>Purchase</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="purchase_add.php"><i class="fa fa-circle-o"></i>Purchase Order</a></li>
                <li><a href="purchase_list.php"><i class="fa fa-circle-o"></i>Purchase Transaction List</a></li>
                
              </ul>
              
              </li>
            
           </ul>
           
           
        </section>
        <!-- /.sidebar -->
      </aside>

    