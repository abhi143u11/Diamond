<?php
require_once('class.database.php');



class Ds_Purchase
{
private $pid;
private $pcid;
private $customername;
private $broker;
private $brokeragetype;
private $brokeragevalue;
private $brokerageamount;
private $amount;
private $pdate;
private $no_of_days;
private $duedate;
private $rate;
private $weight;
private $weighttype;
private $type;
private $status;  
    
/*
* constructor declared 
*/
public function Ds_purchase($pid = '')
    {
        if ($pid != '') {
            if($this->set_pid($pid)){
                if ($this->load_purchase())
                    return TRUE;
                else
                    return FALSE;
            } else
                return FALSE;
        }
    }
    
    public function __destruct()
    {
        // TODO: destructor code
    }

/*
*  load all purchase data 
*/    
public function load_purchase()
    {
        global $database;
        $query = "SELECT * FROM `purchase` WHERE `pid`= '" . $this->pid . "'";
        $result = $database->query_fetch_full_result($query);
        
        if (!$result) {
            return FALSE;
        }
 
        $result            = $result[0];
        $this->pid = $result['pid'];
        $this->pcid    = $result['pcid'];
        $this->broker    = $result['broker'];
        $this->brokeragetype    = $result['brokeragetype'];
        $this->brokeragevalue    = $result['brokeragevalue'];
        $this->brokerageamount    = $result['brokerageamount'];
      
        $this->amount     = $result['amount'];
        $this->pdate = date('d/m/Y',strtotime($result['pdate']));
        $this->no_of_days = $result['no_of_days'];
        $this->duedate    = $result['duedate'];
        $this->rate     = $result['rate'];
        $this->weight     = $result['weight'];
        $this->weighttype = $result['weighttype'];
        $this->type     = $result['type'];
        $this->status = $result['status'];
        return TRUE;
    }    

/*
* Add Purchase transaction 
*/

public function add_purchase()
{
        global $database;
        $ary_err    = array();
        $ary_return = array();
   
        if ($this->customername = '') {
            $ary_err[] = 'Product customername not entered';
        }
        
        if (count($ary_err) > 0) {
            $ary_return["success"] = FALSE;
            $ary_err["errors"]     = $ary_err;
            return $ary_return;
        }
 
      //  $myArray = explode('/', $this->pdate);
       // $pdate=$myArray[2]."-".$myArray[0]."-".$myArray[1];
        $actualdate=$pdate;
        
        $no_of_days = $this->no_of_days * 86400;
    //    $pdate = strtotime($this->pdate);
        $total = $pdate + $no_of_days;
        $duedate = date('Y-m-d', $total);
        $amount=$this->rate*$this->weight;
   
        if($this->brokeragetype=="PERCENTAGE")
        {
           $brokerageamount=($amount * $this->brokeragevalue)/100;
           $amount=$amount-$brokerageamount;
           $this->brokerageamount=$brokerageamount;
        }
        else
        {
            $brokerageamount=$this->brokeragevalue;
            $amount=$amount-$brokerageamount;
            $this->brokerageamount=$brokerageamount;    
        }
        
        $this->amount=$amount;
        $query = "INSERT INTO `purchase`(`pcid`,`broker`,`brokeragetype`,`brokeragevalue`,`brokerageamount`,`amount`,`pdate`, `no_of_days`, `duedate`, `rate`, `weight`, `weighttype`, `type`, `status`)    VALUES (" . $this->pcid . ",".$this->broker.",'".$this->brokeragetype."',".$this->brokeragevalue.",".$this->brokerageamount."," . $this->amount . ",'" . $actualdate . "','" . $this->no_of_days . "','" . $duedate . "'," . $this->rate . "," . $this->weight . ",'" . $this->weighttype . "','" . $this->type . "'," . $this->status . ")";
 
        $result = $database->query($query);
        echo $result;
        if ($result) {
            $ary_return["success"] = TRUE;
            return $ary_return;
        }
        $ary_return["success"] = FALSE;
        $ary_err["errors"]     = $ary_err;
        return $ary_return;    
}

/*
*  get Purchase Transaction List 
* 
*/
     public function all_list_purchase()
     {
         global $database;
         $query="SELECT * FROM PURCHASE";
         $result=$database->query_fetch_full_result($query);
         return $result;     
     }

/*
*  update purchase transaction 
*/
 public function update_purchase()
     {
    global $database;
        $ary_err    = array();
        $ary_return = array();
   
        if ($this->customername = '') {
            $ary_err[] = 'Product customername not entered';
        }
        
        if (count($ary_err) > 0) {
            $ary_return["success"] = FALSE;
            $ary_err["errors"]     = $ary_err;
            return $ary_return;
        }
 
    //    $myArray = explode('/', $this->pdate);
   //     $pdate=$myArray[2]."-".$myArray[0]."-".$myArray[1];
        $actualdate=$pdate;
        
        $no_of_days = $this->no_of_days * 86400;
        //$pdate = strtotime($this->pdate);
        $total = $pdate + $no_of_days;
        $duedate = date('Y-m-d', $total);
        $amount=$this->rate*$this->weight;
   
        if($this->brokeragetype=="PERCENTAGE")
        {
           $brokerageamount=($amount * $this->brokeragevalue)/100;
           $amount=$amount-$brokerageamount;
           $this->brokerageamount=$brokerageamount;
        }
        else
        {
            $brokerageamount=$this->brokeragevalue;
            $amount=$amount-$brokerageamount;
            $this->brokerageamount=$brokerageamount;    
        }
        
        $this->amount=$amount;
        $query = "UPDATE  `purchase` SET `pcid`=".$this->pcid.",`broker`=".$this->broker.",`brokeragetype`='".$this->brokeragetype."',`brokeragevalue`=".$this->brokeragevalue.",`brokerageamount`=".$this->brokerageamount.",`amount`=".$this->amount.",`pdate`='".$actualdate ."', `no_of_days`='".$this->no_of_days."', `duedate`='".$duedate."', `rate`=".$this->rate.", `weight`=".$this->weight.", `weighttype`='".$this->weighttype."', `type`='".$this->type."', `status`=".$this->status." WHERE `pid`=".$this->pid."";
 
 
    var_dump($query);
    exit;
 
        $result = $database->query($query);
        
        echo $result;
        if ($result) {
            $ary_return["success"] = TRUE;
            return $ary_return;
        }
        $ary_return["success"] = FALSE;
        $ary_err["errors"]     = $ary_err;
        return $ary_return;      
     }

    
/*
*  Delete Purchase Order  
*/
public function delete_purchase()
    {
        global $database;
        $ary_err    = array();
        $ary_return = array();
        
        $query      = "DELETE FROM PURCHASE WHERE `pid`=" . $this->pid ."";
        
        $result = $database->query($query);
        
        if ($result) {
            $ary_return["success"] = TRUE;
            return $ary_return;
        }
        
        $ary_return[]      = FALSE;
        $ary_err["errors"] = $ary_err;
        return $ary_return;
        
    }
/*
* Update Purchase transaction 
*/

    
/*
* purchase setter and getter method 
*/    

    public function get_pid(){
        return $this->pid;
    }

    public function set_pid($pid){
        $this->pid = $pid;
        $this->load_purchase();
    }

    public function get_pcid(){
        return $this->pcid;
    }

    public function set_pcid($pcid){
        $this->pcid = $pcid;
    }


    public function get_amount(){
        return $this->amount;
    }

    public function set_amount($amount){
        $this->amount = $amount;
    }

    public function get_pdate(){
        return date('d/m/Y',strtotime($this->pdate));
    }

    public function set_pdate($pdate){
        $this->pdate = strtotime($pdate);
    }

    public function get_no_of_days(){
        return $this->no_of_days;
    }

    public function set_no_of_days($no_of_days){
        $this->no_of_days = $no_of_days;
    }

    public function get_duedate(){
        return $this->duedate;
    }

    public function set_duedate($duedate){
        $this->duedate = $duedate;
    }

    public function get_rate(){
        return $this->rate;
    }

    public function set_rate($rate){
        $this->rate = $rate;
    }

    public function get_weight(){
        return $this->weight;
    }

    public function set_weight($weight){
        $this->weight = $weight;
    }

    public function get_weighttype(){
        return $this->weighttype;
    }

    public function set_weighttype($weighttype){
        $this->weighttype = $weighttype;
    }

    public function get_type(){
        return $this->type;
    }

    public function set_type($type){
        $this->type = $type;
    }

    public function get_status(){
        return $this->status;
    }

    public function set_status($status){
        $this->status = $status;
    }
    
    
        public function get_broker(){
        return $this->broker;
    }

    public function set_broker($broker){
        $this->broker = $broker;
    }



    public function get_brokeragevalue(){
        return $this->brokeragevalue;
    }

    public function set_brokeragevalue($brokeragevalue){
        $this->brokeragevalue = $brokeragevalue;
    }

    public function get_brokerageamount(){
        return $this->brokerageamount;
    }

    public function set_brokerageamount($brokerageamount){
        $this->brokerageamount = $brokerageamount;
    }
    
    public function get_brokeragetype(){
        return $this->brokeragetype;
    }

    public function set_brokeragetype($brokeragetype){
        $this->brokeragetype = $brokeragetype;
    }
    
  
}    
?>
